# Elevada POC Spark Cluster
=====

This repository contains the development environment for the Elevada POC,
it requires some source files to be downloaded, see: downloads/download.sh

Usage:

    vagrant up

Once the virtual machine is up, go into the vm and set up the Cluster

    vagrant ssh spark-master-node
    sudo bash
    /usr/local/hadoop/bin/hdfs namenode -format myhadoop
    /usr/local/hadoop/sbin/hadoop-daemon.sh --config /usr/local/hadoop/etc/hadoop --script hdfs start namenode
    /usr/local/hadoop/sbin/hadoop-daemons.sh --config /usr/local/hadoop/etc/hadoop --script hdfs start datanode

Set up YARN (on spark-node-2):

    /usr/local/hadoop/sbin/yarn-daemon.sh --config /usr/local/hadoop/etc/hadoop start resourcemanager
    /usr/local/hadoop/sbin/yarn-daemons.sh --config /usr/local/hadoop/etc/hadoop start nodemanager
    /usr/local/hadoop/sbin/yarn-daemon.sh start proxyserver --config /usr/local/hadoop/etc/hadoop
    /usr/local/hadoop/sbin/mr-jobhistory-daemon.sh start historyserver --config /usr/local/hadoop/etc/hadoop

Finally start Spark:

    /usr/local/spark/sbin/start-all.sh


Check the Spark UI at: http://spark-node-0:8080/

Once all services are up, run unit test:

    export SLACK_API_TOKEN=xxxx-XXXXXXXXXX-XXXXXXXXXX-XXXXXXXXXX-XXXXXXXXXX
    /usr/local/spark/bin/spark-submit --master spark://spark-node-0:7077 --num-executors 5 --executor-cores 1 /home/centos/idp-spark-cluster/files/example.py

