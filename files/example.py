import unittest2
import logging
import os
from pyspark import SparkConf, SparkContext
from slacker import Slacker

APP_NAME = "SparkExampleTestApp"
SLACK_API_TOKEN = os.getenv('SLACK_API_TOKEN')

class ExampleTest(unittest2.TestCase):

    def setUp(self):
        conf = SparkConf().setAppName(APP_NAME)
        self.sc = SparkContext(conf=conf)
        #quiet_logs(self.sc)

    def tearDown(self):
        self.sc.stop()

    def test_something(self):
        # start by creating a mockup dataset
        l = [(1, 'hello'), (2, 'world'), (3, 'world')]
        # and create a RDD out of it
        rdd = self.sc.parallelize(l)
        # pass it to the transformation you're unit testing
        result = non_trivial_transform(rdd)
        # collect the results
        output = result.collect()
        # sending alert to slack
        slack = Slacker(SLACK_API_TOKEN)
        slack.chat.post_message('#idp-notification', 'Hello from Spark!')
        # since it's unit test let's make an assertion
        self.assertEqual(output[0][1], 2)


def non_trivial_transform(rdd):
    """ a transformation to unit test (word count) - defined here for convenience only"""
    return rdd.map(lambda x: (x[1], 1)).reduceByKey(lambda a, b: a + b)

if __name__ == "__main__":
    unittest2.main()