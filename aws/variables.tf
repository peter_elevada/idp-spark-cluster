# aws credentials for provider (update terraform.tfvars)
variable "aws_access_key" {}
variable "aws_secret_key" {}
variable "aws_region" {}

# ip addresses referenced in security groups
variable "ip_ernesto" {}
variable "ip_javier" {}
variable "ip_peter" {}

variable "ami_centos" {
    default = "ami-d2c924b2"
}
variable "instance_type_spark" {
    default = "m3.medium"
}
variable "key_name_spark" {
    default = "keypair14"
}
variable "instance_type_activemq" {
    default = "t2.small"
}
variable "key_name_activemq" {
    default = "keypair14"
}
