#!/bin/bash
activemq="/tmp/vagrant.activemq"
if [ -f "$activemq" ]
then
    echo "this node has already been activemq, file found: $activemq"
else
    echo "install and configure activemq 5.9.0"
    tar -xzf /home/centos/idp-spark-cluster/downloads/apache-activemq-5.9.0-bin.tar.gz -C /opt
    ln -s /opt/apache-activemq-5.9.0 /opt/activemq
    adduser --system activemq
    chown -R activemq: /opt/apache-activemq-5.9.0
    /opt/activemq/bin/activemq start
    curl -u admin:admin -d "{\"type\":\"exec\",\"mbean\":\"org.apache.activemq:type=Broker,brokerName=localhost\",\"operation\":\"addQueue(java.lang.String)\",\"arguments\":[\"job_processing\"]}" http://localhost:8161/hawtio/jolokia/
    curl -u admin:admin -d "{\"type\":\"exec\",\"mbean\":\"org.apache.activemq:type=Broker,brokerName=localhost\",\"operation\":\"addTopic(java.lang.String)\",\"arguments\":[\"notifications\"]}" http://localhost:8161/hawtio/jolokia/
    touch "$activemq"
fi