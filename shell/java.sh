#!/bin/bash
javad="/tmp/vagrant.javad"
if [ -f "$javad" ]
then
    echo "java has already been installed in this node, file found: $javad"
else
    echo "install and configure java jdk"
    tar -xzf /home/centos/idp-spark-cluster/downloads/jdk-8u74-linux-i586.tar.gz -C /usr/local
    ln -s /usr/local/jdk1.8.0_74 /usr/local/java
    cp -f /home/centos/idp-spark-cluster/files/etc/profile.d/java.sh /etc/profile.d/java.sh
    touch "$javad"
fi