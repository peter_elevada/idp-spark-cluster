#!/bin/bash
hadoopd="/tmp/vagrant.hadoopd"
if [ -f "$hadoopd" ]
then
    echo "this node has already been hadoopd, file found: $hadoopd"
else
    echo "install and configure hadoop 2.6.4"
    tar -xzf /home/centos/idp-spark-cluster/downloads/hadoop-2.6.4.tar.gz -C /usr/local
    mkdir -p /var/hadoop/hadoop-datanode
    mkdir -p /var/hadoop/hadoop-namenode
    mkdir -p /var/hadoop/mr-history/done
    mkdir -p /var/hadoop/mr-history/tmp
    ln -s /usr/local/hadoop-2.6.4 /usr/local/hadoop
    cp -f /home/centos/idp-spark-cluster/files/hadoop/etc/hadoop/core-site.xml /usr/local/hadoop/etc/hadoop/core-site.xml
    cp -f /home/centos/idp-spark-cluster/files/hadoop/etc/hadoop/hadoop-env.sh /usr/local/hadoop/etc/hadoop/hadoop-env.sh
    cp -f /home/centos/idp-spark-cluster/files/hadoop/etc/hadoop/hadoop.sh /usr/local/hadoop/etc/hadoop/hadoop.sh
    cp -f /home/centos/idp-spark-cluster/files/hadoop/etc/hadoop/hdfs-site.xml /usr/local/hadoop/etc/hadoop/hdfs-site.xml
    cp -f /home/centos/idp-spark-cluster/files/hadoop/etc/hadoop/mapred-env.sh /usr/local/hadoop/etc/hadoop/mapred-env.sh
    cp -f /home/centos/idp-spark-cluster/files/hadoop/etc/hadoop/mapred-site.xml /usr/local/hadoop/etc/hadoop/mapred-site.xml
    cp -f /home/centos/idp-spark-cluster/files/hadoop/etc/hadoop/yarn-env.sh /usr/local/hadoop/etc/hadoop/yarn-env.sh
    cp -f /home/centos/idp-spark-cluster/files/hadoop/etc/hadoop/yarn-site.xml /usr/local/hadoop/etc/hadoop/yarn-site.xml
    cp -f /home/centos/idp-spark-cluster/files/hadoop/etc/hadoop/slaves /usr/local/hadoop/etc/hadoop/slaves
    cp -f /home/centos/idp-spark-cluster/files/etc/profile.d/hadoop.sh /etc/profile.d/hadoop.sh
    touch "$hadoopd"
fi