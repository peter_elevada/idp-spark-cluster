#!/bin/bash
sparked="/tmp/vagrant.sparked"
if [ -f "$sparked" ]
then
    echo "this node has already been sparked, file found: $sparked"
else
    echo "install and configure spark 1.6.1"
    tar -xzf /home/centos/idp-spark-cluster/downloads/spark-1.6.1-bin-hadoop2.6.tgz -C /usr/local
    ln -s /usr/local/spark-1.6.1-bin-hadoop2.6 /usr/local/spark
    mkdir -p /usr/local/spark/conf
    cp -f /home/centos/idp-spark-cluster/files/spark/conf/spark-env.sh /usr/local/spark/conf/spark-env.sh
    cp -f /home/centos/idp-spark-cluster/files/spark/conf/slaves /usr/local/spark/conf/slaves
    cp -f /home/centos/idp-spark-cluster/files/etc/profile.d/spark.sh /etc/profile.d/spark.sh
    touch "$sparked"
fi